<?php require_once('Connections/conf.php'); ?>
<?php
//@@UrlFormat@@('Connections/conf.php'); 

// Load the common classes
require_once('includes/common/KT_common.php');

// Load the tNG classes
require_once('includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("");

// Make unified connection variable
$conn_conf = new KT_connection($conf, $database_conf);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("hoten", true, "text", "", "", "", "Please insert your name here");
$formValidation->addField("email", true, "text", "", "", "", "Please enter your email address");
$formValidation->addField("noidung", true, "text", "", "", "", "Please enter a content");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_detail = "-1";
if (isset($_GET['id'])) {
  $colname_detail = $_GET['id'];
}
mysql_select_db($database_conf, $conf);
$query_detail = sprintf("SELECT * FROM products WHERE idSP = %s", GetSQLValueString($colname_detail, "int"));
$detail = mysql_query($query_detail, $conf) or die(mysql_error());
$row_detail = mysql_fetch_assoc($detail);
$totalRows_detail = mysql_num_rows($detail);

// Make an insert transaction instance
$ins_products_cm = new tNG_insert($conn_conf);
$tNGs->addTransaction($ins_products_cm);
// Register triggers
$ins_products_cm->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_products_cm->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_products_cm->registerTrigger("END", "Trigger_Default_Redirect", 99, "index.php?act=product&idSex={$row_detail['idSex']}");
// Add columns
$ins_products_cm->setTable("products_cm");
$ins_products_cm->addColumn("idSP", "NUMERIC_TYPE", "GET", "id");
$ins_products_cm->addColumn("noidung", "STRING_TYPE", "POST", "noidung");
$ins_products_cm->addColumn("idLogin", "NUMERIC_TYPE", "SESSION", "kt_login_user");
$ins_products_cm->addColumn("ngay_comment", "DATE_TYPE", "POST", "ngay_comment");
$ins_products_cm->addColumn("kiem_duyet", "STRING_TYPE", "POST", "kiem_duyet");
$ins_products_cm->setPrimaryKey("id_comment", "NUMERIC_TYPE");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsproducts_cm = $tNGs->getRecordset("products_cm");
$row_rsproducts_cm = mysql_fetch_assoc($rsproducts_cm);
$totalRows_rsproducts_cm = mysql_num_rows($rsproducts_cm);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="css/index.css" rel="stylesheet" type="text/css" />
<link href="includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="includes/common/js/base.js" type="text/javascript"></script>
<script src="includes/common/js/utility.js" type="text/javascript"></script>
<script src="includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>

</head>

<body>
<div id="detail">
  <p align="left"><img src="images/<?php echo $row_detail['UrlHinh']; ?>" /></p>
  <p align="center" class="style1"><?php echo $row_detail['TenSP']; ?></p>
  <p align="center" class="style1"><?php echo $row_detail['MoTa']; ?> </p>
  <p align="center"><span class="style1">Giá:<?php echo $row_detail['Gia']; ?> VNĐ</span></p>
  <p align="center" class="style1">Ý kiến bạn đọc:</p>
  <p align="center" class="style1">
  <label>
<form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
  <table cellpadding="2" cellspacing="0" class="KT_tngtable">
    <tr>
      <td><label for="noidung">:</label>
      <form>
        <textarea id="ykien" name="noidung" id="noidung" cols="50" rows="5"><?php echo KT_escapeAttribute($row_rsproducts_cm['noidung']); ?> </textarea>
          <?php echo $tNGs->displayFieldHint("noidung");?> 
      <input type="submit" name="KT_Insert1" id="KT_Insert1" value="Send " />          <?php echo $tNGs->displayFieldError("products_cm", "noidung"); ?> </td>
    </tr>
  </table>
  <input name="idSP" type="hidden" id="idSP" value="<?php echo $row_detail['idSP']; ?>" />
  <input name="tieude" type="hidden" id="tieude" value="<?php echo $row_rsproducts_cm['tieude']; ?>" />
  <input type="hidden" name="ngay_comment" id="ngay_comment" value="<?php echo Date('Y,m,d'); ?>" />
  <input name="kiem_duyet" type="hidden" id="kiem_duyet" value="1" />
  </form>
    </label>
</div>
</body>
</html>
<?php
mysql_free_result($detail);
?>
