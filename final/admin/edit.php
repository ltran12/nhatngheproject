<?php require_once('../Connections/conf.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_conf = new KT_connection($conf, $database_conf);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an insert transaction instance
$ins_products = new tNG_multipleInsert($conn_conf);
$tNGs->addTransaction($ins_products);
// Register triggers
$ins_products->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_products->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_products->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$ins_products->setTable("products");
$ins_products->addColumn("idLoai", "NUMERIC_TYPE", "POST", "idLoai");
$ins_products->addColumn("idNSX", "NUMERIC_TYPE", "POST", "idNSX");
$ins_products->addColumn("TenSP", "STRING_TYPE", "POST", "TenSP");
$ins_products->addColumn("MoTa", "STRING_TYPE", "POST", "MoTa");
$ins_products->addColumn("NgayCapNhat", "DATE_TYPE", "POST", "NgayCapNhat");
$ins_products->addColumn("Gia", "NUMERIC_TYPE", "POST", "Gia");
$ins_products->addColumn("UrlHinh", "STRING_TYPE", "POST", "UrlHinh");
$ins_products->addColumn("baiviet", "STRING_TYPE", "POST", "baiviet");
$ins_products->addColumn("SoLanXem", "NUMERIC_TYPE", "POST", "SoLanXem");
$ins_products->addColumn("SoLuongTonKho", "NUMERIC_TYPE", "POST", "SoLuongTonKho");
$ins_products->addColumn("GhiChu", "STRING_TYPE", "POST", "GhiChu");
$ins_products->addColumn("SoLanMua", "NUMERIC_TYPE", "POST", "SoLanMua");
$ins_products->addColumn("AnHien", "NUMERIC_TYPE", "POST", "AnHien");
$ins_products->addColumn("idSex", "NUMERIC_TYPE", "POST", "idSex");
$ins_products->setPrimaryKey("idSP", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_products = new tNG_multipleUpdate($conn_conf);
$tNGs->addTransaction($upd_products);
// Register triggers
$upd_products->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_products->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_products->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$upd_products->setTable("products");
$upd_products->addColumn("idLoai", "NUMERIC_TYPE", "POST", "idLoai");
$upd_products->addColumn("idNSX", "NUMERIC_TYPE", "POST", "idNSX");
$upd_products->addColumn("TenSP", "STRING_TYPE", "POST", "TenSP");
$upd_products->addColumn("MoTa", "STRING_TYPE", "POST", "MoTa");
$upd_products->addColumn("NgayCapNhat", "DATE_TYPE", "POST", "NgayCapNhat");
$upd_products->addColumn("Gia", "NUMERIC_TYPE", "POST", "Gia");
$upd_products->addColumn("UrlHinh", "STRING_TYPE", "POST", "UrlHinh");
$upd_products->addColumn("baiviet", "STRING_TYPE", "POST", "baiviet");
$upd_products->addColumn("SoLanXem", "NUMERIC_TYPE", "POST", "SoLanXem");
$upd_products->addColumn("SoLuongTonKho", "NUMERIC_TYPE", "POST", "SoLuongTonKho");
$upd_products->addColumn("GhiChu", "STRING_TYPE", "POST", "GhiChu");
$upd_products->addColumn("SoLanMua", "NUMERIC_TYPE", "POST", "SoLanMua");
$upd_products->addColumn("AnHien", "NUMERIC_TYPE", "POST", "AnHien");
$upd_products->addColumn("idSex", "NUMERIC_TYPE", "POST", "idSex");
$upd_products->setPrimaryKey("idSP", "NUMERIC_TYPE", "GET", "idSP");

// Make an instance of the transaction object
$del_products = new tNG_multipleDelete($conn_conf);
$tNGs->addTransaction($del_products);
// Register triggers
$del_products->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_products->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$del_products->setTable("products");
$del_products->setPrimaryKey("idSP", "NUMERIC_TYPE", "GET", "idSP");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsproducts = $tNGs->getRecordset("products");
$row_rsproducts = mysql_fetch_assoc($rsproducts);
$totalRows_rsproducts = mysql_num_rows($rsproducts);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: true,
  show_as_grid: true,
  merge_down_value: true
}
</script>
</head>

<body>
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    <?php 
// Show IF Conditional region1 
if (@$_GET['idSP'] == "") {
?>
      <?php echo NXT_getResource("Insert_FH"); ?>
      <?php 
// else Conditional region1
} else { ?>
      <?php echo NXT_getResource("Update_FH"); ?>
      <?php } 
// endif Conditional region1
?>
    Products </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php do { ?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsproducts > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <tr>
            <td class="KT_th"><label for="TenSP_<?php echo $cnt1; ?>">TenSP:</label></td>
            <td><input type="text" name="TenSP_<?php echo $cnt1; ?>" id="TenSP_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsproducts['TenSP']); ?>" size="32" maxlength="100" />
                <?php echo $tNGs->displayFieldHint("TenSP");?> <?php echo $tNGs->displayFieldError("products", "TenSP", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="MoTa_<?php echo $cnt1; ?>">MoTa:</label></td>
            <td><input type="text" name="MoTa_<?php echo $cnt1; ?>" id="MoTa_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsproducts['MoTa']); ?>" size="32" maxlength="255" />
                <?php echo $tNGs->displayFieldHint("MoTa");?> <?php echo $tNGs->displayFieldError("products", "MoTa", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="NgayCapNhat_<?php echo $cnt1; ?>">NgayCapNhat:</label></td>
            <td><input type="text" name="NgayCapNhat_<?php echo $cnt1; ?>" id="NgayCapNhat_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsproducts['NgayCapNhat']); ?>" size="10" maxlength="22" />
                <?php echo $tNGs->displayFieldHint("NgayCapNhat");?> <?php echo $tNGs->displayFieldError("products", "NgayCapNhat", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="Gia_<?php echo $cnt1; ?>">Gia:</label></td>
            <td><input type="text" name="Gia_<?php echo $cnt1; ?>" id="Gia_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsproducts['Gia']); ?>" size="4" />
                <?php echo $tNGs->displayFieldHint("Gia");?> <?php echo $tNGs->displayFieldError("products", "Gia", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="UrlHinh_<?php echo $cnt1; ?>">UrlHinh:</label></td>
            <td><input type="text" name="UrlHinh_<?php echo $cnt1; ?>" id="UrlHinh_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsproducts['UrlHinh']); ?>" size="32" maxlength="255" />
                <?php echo $tNGs->displayFieldHint("UrlHinh");?> <?php echo $tNGs->displayFieldError("products", "UrlHinh", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="baiviet_<?php echo $cnt1; ?>">Baiviet:</label></td>
            <td><input type="text" name="baiviet_<?php echo $cnt1; ?>" id="baiviet_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsproducts['baiviet']); ?>" size="32" />
                <?php echo $tNGs->displayFieldHint("baiviet");?> <?php echo $tNGs->displayFieldError("products", "baiviet", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="SoLanXem_<?php echo $cnt1; ?>">SoLanXem:</label></td>
            <td><input type="text" name="SoLanXem_<?php echo $cnt1; ?>" id="SoLanXem_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsproducts['SoLanXem']); ?>" size="4" />
                <?php echo $tNGs->displayFieldHint("SoLanXem");?> <?php echo $tNGs->displayFieldError("products", "SoLanXem", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="SoLuongTonKho_<?php echo $cnt1; ?>">SoLuongTonKho:</label></td>
            <td><input type="text" name="SoLuongTonKho_<?php echo $cnt1; ?>" id="SoLuongTonKho_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsproducts['SoLuongTonKho']); ?>" size="4" />
                <?php echo $tNGs->displayFieldHint("SoLuongTonKho");?> <?php echo $tNGs->displayFieldError("products", "SoLuongTonKho", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="GhiChu_<?php echo $cnt1; ?>">GhiChu:</label></td>
            <td><input type="text" name="GhiChu_<?php echo $cnt1; ?>" id="GhiChu_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsproducts['GhiChu']); ?>" size="32" maxlength="255" />
                <?php echo $tNGs->displayFieldHint("GhiChu");?> <?php echo $tNGs->displayFieldError("products", "GhiChu", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="SoLanMua_<?php echo $cnt1; ?>">SoLanMua:</label></td>
            <td><input type="text" name="SoLanMua_<?php echo $cnt1; ?>" id="SoLanMua_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsproducts['SoLanMua']); ?>" size="4" />
                <?php echo $tNGs->displayFieldHint("SoLanMua");?> <?php echo $tNGs->displayFieldError("products", "SoLanMua", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="AnHien_<?php echo $cnt1; ?>">AnHien:</label></td>
            <td><input type="text" name="AnHien_<?php echo $cnt1; ?>" id="AnHien_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsproducts['AnHien']); ?>" size="2" />
                <?php echo $tNGs->displayFieldHint("AnHien");?> <?php echo $tNGs->displayFieldError("products", "AnHien", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="idSex_<?php echo $cnt1; ?>">idSex:</label></td>
            <td><select name="idSex_<?php echo $cnt1; ?>" id="idSex_<?php echo $cnt1; ?>">
              <option value="1" <?php if (!(strcmp(1, KT_escapeAttribute($row_rsproducts['idSex'])))) {echo "SELECTED";} ?>>Nam</option>
              <option value="0" <?php if (!(strcmp(0, KT_escapeAttribute($row_rsproducts['idSex'])))) {echo "SELECTED";} ?>>Nữ</option>
            </select>
                <?php echo $tNGs->displayFieldError("products", "idSex", $cnt1); ?> </td>
          </tr>
        </table>
        <input type="hidden" name="kt_pk_products_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsproducts['kt_pk_products']); ?>" />
        <?php } while ($row_rsproducts = mysql_fetch_assoc($rsproducts)); ?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1
      if (@$_GET['idSP'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <div class="KT_operations">
              <input type="submit" name="KT_Insert1" value="<?php echo NXT_getResource("Insert as new_FB"); ?>" onclick="nxt_form_insertasnew(this, 'idSP')" />
            </div>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
            <?php }
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</body>
</html>
