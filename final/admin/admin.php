<?php require_once('../Connections/conf.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Make unified connection variable
$conn_conf = new KT_connection($conf, $database_conf);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listproducts1 = new TFI_TableFilter($conn_conf, "tfi_listproducts1");
$tfi_listproducts1->addColumn("products.TenSP", "STRING_TYPE", "TenSP", "%");
$tfi_listproducts1->addColumn("products.MoTa", "STRING_TYPE", "MoTa", "%");
$tfi_listproducts1->addColumn("products.NgayCapNhat", "DATE_TYPE", "NgayCapNhat", "=");
$tfi_listproducts1->addColumn("products.Gia", "NUMERIC_TYPE", "Gia", "=");
$tfi_listproducts1->addColumn("products.UrlHinh", "STRING_TYPE", "UrlHinh", "%");
$tfi_listproducts1->addColumn("products.baiviet", "STRING_TYPE", "baiviet", "%");
$tfi_listproducts1->addColumn("products.idSex", "NUMERIC_TYPE", "idSex", "=");
$tfi_listproducts1->addColumn("products.GhiChu", "STRING_TYPE", "GhiChu", "%");
$tfi_listproducts1->Execute();

// Sorter
$tso_listproducts1 = new TSO_TableSorter("rsproducts1", "tso_listproducts1");
$tso_listproducts1->addColumn("products.TenSP");
$tso_listproducts1->addColumn("products.MoTa");
$tso_listproducts1->addColumn("products.NgayCapNhat");
$tso_listproducts1->addColumn("products.Gia");
$tso_listproducts1->addColumn("products.UrlHinh");
$tso_listproducts1->addColumn("products.baiviet");
$tso_listproducts1->addColumn("products.idSex");
$tso_listproducts1->addColumn("products.GhiChu");
$tso_listproducts1->setDefault("products.TenSP");
$tso_listproducts1->Execute();

// Navigation
$nav_listproducts1 = new NAV_Regular("nav_listproducts1", "rsproducts1", "", $_SERVER['PHP_SELF'], 10);

//NeXTenesio3 Special List Recordset
$maxRows_rsproducts1 = $_SESSION['max_rows_nav_listproducts1'];
$pageNum_rsproducts1 = 0;
if (isset($_GET['pageNum_rsproducts1'])) {
  $pageNum_rsproducts1 = $_GET['pageNum_rsproducts1'];
}
$startRow_rsproducts1 = $pageNum_rsproducts1 * $maxRows_rsproducts1;

// Defining List Recordset variable
$NXTFilter_rsproducts1 = "1=1";
if (isset($_SESSION['filter_tfi_listproducts1'])) {
  $NXTFilter_rsproducts1 = $_SESSION['filter_tfi_listproducts1'];
}
// Defining List Recordset variable
$NXTSort_rsproducts1 = "products.TenSP";
if (isset($_SESSION['sorter_tso_listproducts1'])) {
  $NXTSort_rsproducts1 = $_SESSION['sorter_tso_listproducts1'];
}
mysql_select_db($database_conf, $conf);

$query_rsproducts1 = "SELECT products.TenSP, products.MoTa, products.NgayCapNhat, products.Gia, products.UrlHinh, products.baiviet, products.idSex, products.GhiChu, products.idSP FROM products WHERE {$NXTFilter_rsproducts1} ORDER BY {$NXTSort_rsproducts1}";
$query_limit_rsproducts1 = sprintf("%s LIMIT %d, %d", $query_rsproducts1, $startRow_rsproducts1, $maxRows_rsproducts1);
$rsproducts1 = mysql_query($query_limit_rsproducts1, $conf) or die(mysql_error());
$row_rsproducts1 = mysql_fetch_assoc($rsproducts1);

if (isset($_GET['totalRows_rsproducts1'])) {
  $totalRows_rsproducts1 = $_GET['totalRows_rsproducts1'];
} else {
  $all_rsproducts1 = mysql_query($query_rsproducts1);
  $totalRows_rsproducts1 = mysql_num_rows($all_rsproducts1);
}
$totalPages_rsproducts1 = ceil($totalRows_rsproducts1/$maxRows_rsproducts1)-1;
//End NeXTenesio3 Special List Recordset

$nav_listproducts1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- DW6 -->
<head>
<!-- Copyright 2005 Macromedia, Inc. All rights reserved. -->
<title>Restaurant - Home Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../mm_restaurant1.css" type="text/css" />
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: true,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: true,
  record_counter: true
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_TenSP {width:140px; overflow:hidden;}
  .KT_col_MoTa {width:140px; overflow:hidden;}
  .KT_col_NgayCapNhat {width:140px; overflow:hidden;}
  .KT_col_Gia {width:140px; overflow:hidden;}
  .KT_col_UrlHinh {width:140px; overflow:hidden;}
  .KT_col_baiviet {width:140px; overflow:hidden;}
  .KT_col_idSex {width:140px; overflow:hidden;}
  .KT_col_GhiChu {width:140px; overflow:hidden;}
.style2 {
	font-size: 16px;
	color: #FF0000;
}
</style>
</head>
<body bgcolor="#0066cc">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#99ccff">
	<td width="230" nowrap="nowrap" ><img src="../mm_spacer.gif" alt="" width="15" height="1" border="0" /></td>
	<td height="60" colspan="3" class="logo" nowrap="nowrap"><br />	  <span class="tagline"></span></td>
	<td width="40">&nbsp;</td>
	<td width="100%">&nbsp;</td>
	</tr>

	<tr bgcolor="#003399">
	<td height="36" colspan="6" nowrap="nowrap" bgcolor="#FFFF00"><a href="javascript:;" class="style2">PRODUCT</a></td>
	</tr>

	<tr bgcolor="#ffffff">
	<td colspan="6"><img src="../mm_spacer.gif" alt="" width="1" height="1" border="0" /></td>
	</tr>

	<tr bgcolor="#ffffff">
	<td colspan="6" valign="top" bgcolor="#ffffcc"><table border="0" cellspacing="0" cellpadding="0" width="15">
		<tr>
		<td width="15">&nbsp;</td>
		</tr>
	</table>	  
      <div class="KT_tng" id="listproducts1">
        <h1> Products
          <?php
  $nav_listproducts1->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?>
        </h1>
        <div class="KT_tnglist">
          <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
            <div class="KT_options"> <a href="<?php echo $nav_listproducts1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
                  <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listproducts1'] == 1) {
?>
                    <?php echo $_SESSION['default_max_rows_nav_listproducts1']; ?>
                    <?php 
  // else Conditional region1
  } else { ?>
                    <?php echo NXT_getResource("all"); ?>
                    <?php } 
  // endif Conditional region1
?>
                  <?php echo NXT_getResource("records"); ?></a> &nbsp;
              &nbsp;
                <?php 
  // Show IF Conditional region2
  if (@$_SESSION['has_filter_tfi_listproducts1'] == 1) {
?>
                  <a href="<?php echo $tfi_listproducts1->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                  <?php 
  // else Conditional region2
  } else { ?>
                  <a href="<?php echo $tfi_listproducts1->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                  <?php } 
  // endif Conditional region2
?>
            </div>
            <table cellpadding="2" cellspacing="0" class="KT_tngtable">
              <thead>
                <tr class="KT_row_order">
                  <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>                  </th>
                  <th id="TenSP" class="KT_sorter KT_col_TenSP <?php echo $tso_listproducts1->getSortIcon('products.TenSP'); ?>"> <a href="<?php echo $tso_listproducts1->getSortLink('products.TenSP'); ?>">TenSP</a> </th>
                  <th id="MoTa" class="KT_sorter KT_col_MoTa <?php echo $tso_listproducts1->getSortIcon('products.MoTa'); ?>"> <a href="<?php echo $tso_listproducts1->getSortLink('products.MoTa'); ?>">MoTa</a> </th>
                  <th id="NgayCapNhat" class="KT_sorter KT_col_NgayCapNhat <?php echo $tso_listproducts1->getSortIcon('products.NgayCapNhat'); ?>"> <a href="<?php echo $tso_listproducts1->getSortLink('products.NgayCapNhat'); ?>">NgayCapNhat</a> </th>
                  <th id="Gia" class="KT_sorter KT_col_Gia <?php echo $tso_listproducts1->getSortIcon('products.Gia'); ?>"> <a href="<?php echo $tso_listproducts1->getSortLink('products.Gia'); ?>">Gia</a> </th>
                  <th id="UrlHinh" class="KT_sorter KT_col_UrlHinh <?php echo $tso_listproducts1->getSortIcon('products.UrlHinh'); ?>"> <a href="<?php echo $tso_listproducts1->getSortLink('products.UrlHinh'); ?>">UrlHinh</a> </th>
                  <th id="baiviet" class="KT_sorter KT_col_baiviet <?php echo $tso_listproducts1->getSortIcon('products.baiviet'); ?>"> <a href="<?php echo $tso_listproducts1->getSortLink('products.baiviet'); ?>">Baiviet</a> </th>
                  <th id="idSex" class="KT_sorter KT_col_idSex <?php echo $tso_listproducts1->getSortIcon('products.idSex'); ?>"> <a href="<?php echo $tso_listproducts1->getSortLink('products.idSex'); ?>">idSex</a> </th>
                  <th id="GhiChu" class="KT_sorter KT_col_GhiChu <?php echo $tso_listproducts1->getSortIcon('products.GhiChu'); ?>"> <a href="<?php echo $tso_listproducts1->getSortLink('products.GhiChu'); ?>">GhiChu</a> </th>
                  <th>&nbsp;</th>
                </tr>
                <?php 
  // Show IF Conditional region3
  if (@$_SESSION['has_filter_tfi_listproducts1'] == 1) {
?>
                  <tr class="KT_row_filter">
                    <td>&nbsp;</td>
                    <td><input type="text" name="tfi_listproducts1_TenSP" id="tfi_listproducts1_TenSP" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listproducts1_TenSP']); ?>" size="20" maxlength="100" /></td>
                    <td><input type="text" name="tfi_listproducts1_MoTa" id="tfi_listproducts1_MoTa" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listproducts1_MoTa']); ?>" size="20" maxlength="255" /></td>
                    <td><input type="text" name="tfi_listproducts1_NgayCapNhat" id="tfi_listproducts1_NgayCapNhat" value="<?php echo @$_SESSION['tfi_listproducts1_NgayCapNhat']; ?>" size="10" maxlength="22" /></td>
                    <td><input type="text" name="tfi_listproducts1_Gia" id="tfi_listproducts1_Gia" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listproducts1_Gia']); ?>" size="20" maxlength="100" /> 
                      VNĐ</td>
                    <td><input type="text" name="tfi_listproducts1_UrlHinh" id="tfi_listproducts1_UrlHinh" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listproducts1_UrlHinh']); ?>" size="20" maxlength="255" /></td>
                    <td><input type="text" name="tfi_listproducts1_baiviet" id="tfi_listproducts1_baiviet" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listproducts1_baiviet']); ?>" size="20" maxlength="100" /></td>
                    <td><select name="tfi_listproducts1_idSex" id="tfi_listproducts1_idSex">
                        <option value="1" <?php if (!(strcmp(1, KT_escapeAttribute(@$_SESSION['tfi_listproducts1_idSex'])))) {echo "SELECTED";} ?>>Nam</option>
                        <option value="0" <?php if (!(strcmp(0, KT_escapeAttribute(@$_SESSION['tfi_listproducts1_idSex'])))) {echo "SELECTED";} ?>>Nữ</option>
                      </select>                    </td>
                    <td><input type="text" name="tfi_listproducts1_GhiChu" id="tfi_listproducts1_GhiChu" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listproducts1_GhiChu']); ?>" size="20" maxlength="255" /></td>
                    <td><input type="submit" name="tfi_listproducts1" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
                  </tr>
                  <?php } 
  // endif Conditional region3
?>
              </thead>
              <tbody>
                <?php if ($totalRows_rsproducts1 == 0) { // Show if recordset empty ?>
                  <tr>
                    <td colspan="10"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
                  </tr>
                  <?php } // Show if recordset empty ?>
                <?php if ($totalRows_rsproducts1 > 0) { // Show if recordset not empty ?>
                  <?php do { ?>
                    <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                      <td><input type="checkbox" name="kt_pk_products" class="id_checkbox" value="<?php echo $row_rsproducts1['idSP']; ?>" />
                          <input type="hidden" name="idSP" class="id_field" value="<?php echo $row_rsproducts1['idSP']; ?>" />                      </td>
                      <td><div class="KT_col_TenSP"><?php echo KT_FormatForList($row_rsproducts1['TenSP'], 20); ?></div></td>
                      <td><div class="KT_col_MoTa"><?php echo KT_FormatForList($row_rsproducts1['MoTa'], 20); ?></div></td>
                      <td><div class="KT_col_NgayCapNhat"><?php echo KT_formatDate($row_rsproducts1['NgayCapNhat']); ?></div></td>
                      <td><div class="KT_col_Gia"><?php echo KT_FormatForList($row_rsproducts1['Gia'], 20); ?></div></td>
                      <td><div class="KT_col_UrlHinh"><?php echo KT_FormatForList($row_rsproducts1['UrlHinh'], 20); ?></div></td>
                      <td><div class="KT_col_baiviet"><?php echo KT_FormatForList($row_rsproducts1['baiviet'], 20); ?></div></td>
                      <td><div class="KT_col_idSex"><?php echo KT_FormatForList($row_rsproducts1['idSex'], 20); ?></div></td>
                      <td><div class="KT_col_GhiChu"><?php echo KT_FormatForList($row_rsproducts1['GhiChu'], 20); ?></div></td>
                      <td><a class="KT_edit_link" href="edit.php?idSP=<?php echo $row_rsproducts1['idSP']; ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> <a class="KT_delete_link" href="#delete"><?php echo NXT_getResource("delete_one"); ?></a> </td>
                    </tr>
                    <?php } while ($row_rsproducts1 = mysql_fetch_assoc($rsproducts1)); ?>
                  <?php } // Show if recordset not empty ?>
              </tbody>
            </table>
            <div class="KT_bottomnav">
              <div>
                <?php
            $nav_listproducts1->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
              </div>
            </div>
            <div class="KT_bottombuttons">
              <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
              <span>&nbsp;</span>
              <select name="no_new" id="no_new">
                <option value="1">1</option>
                <option value="3">3</option>
                <option value="6">6</option>
              </select>
              <a class="KT_additem_op_link" href="edit.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
          </form>
        </div>
        <br class="clearfixplain" />
      </div>
      <p>&nbsp;</p>
      <img src="../mm_spacer.gif" alt="" width="50" height="1" border="0" /></td>
	</tr>

	<tr bgcolor="#ffffff">
	<td colspan="6"><img src="../mm_spacer.gif" alt="" width="1" height="1" border="0" /></td>
	</tr>

	<tr>
	<td colspan="6">&nbsp;</td>
	</tr>


	<tr bgcolor="#003399">
	<td colspan="6"><img src="../mm_spacer.gif" alt="" width="1" height="1" border="0" /></td>
	</tr>

	<tr>
	<td colspan="6"><img src="../mm_spacer.gif" alt="" width="1" height="2" border="0" /></td>
	</tr>

	<tr bgcolor="#003399">
	<td colspan="6"><img src="../mm_spacer.gif" alt="" width="1" height="1" border="0" /></td>
	</tr>


	<tr>
	<td width="230">&nbsp;</td>
	<td width="215">&nbsp;</td>
	<td width="50">&nbsp;</td>
	<td width="440">&nbsp;</td>
	<td width="40">&nbsp;</td>
	<td width="100%">&nbsp;</td>
	</tr>
</table>
</body>
</html>
<?php
mysql_free_result($rsproducts1);
?>
