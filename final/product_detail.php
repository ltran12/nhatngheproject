<?php require_once('Connections/conf.php'); ?>
<?php require_once('Connections/conf.php'); ?>
<?php
//@@UrlFormat@@('Connections/conf.php'); 

// Load the common classes
require_once('includes/common/KT_common.php');

// Load the tNG classes
require_once('includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("");

// Make unified connection variable
$conn_conf = new KT_connection($conf, $database_conf);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("hoten", true, "text", "", "", "", "Please insert your name here");
$formValidation->addField("email", true, "text", "", "", "", "Please enter your email address");
$formValidation->addField("noidung", true, "text", "", "", "", "Please enter a content");
$tNGs->prepareValidation($formValidation);
// End trigger

// Start trigger
$formValidation1 = new tNG_FormValidation();
$formValidation1->addField("noidung", true, "text", "", "", "", "Please enter a content");
$tNGs->prepareValidation($formValidation1);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_detail = "-1";
if (isset($_GET['id'])) {
  $colname_detail = $_GET['id'];
}
mysql_select_db($database_conf, $conf);
$query_detail = sprintf("SELECT * FROM products WHERE idSP = %s", GetSQLValueString($colname_detail, "int"));
$detail = mysql_query($query_detail, $conf) or die(mysql_error());
$row_detail = mysql_fetch_assoc($detail);
$totalRows_detail = mysql_num_rows($detail);

$colname_Login_cm = "-1";
if (isset($_GET['id'])) {
  $colname_Login_cm = $_GET['id'];
}
mysql_select_db($database_conf, $conf);
$query_Login_cm = sprintf("SELECT idSP, noidung, ngay_comment FROM products_cm WHERE idSP = %s AND kiem_duyet = 1 ORDER BY ngay_comment DESC ", GetSQLValueString($colname_Login_cm, "int"));
$Login_cm = mysql_query($query_Login_cm, $conf) or die(mysql_error());
$row_Login_cm = mysql_fetch_assoc($Login_cm);
$totalRows_Login_cm = mysql_num_rows($Login_cm);

// Make an insert transaction instance
$ins_products_cm = new tNG_insert($conn_conf);
$tNGs->addTransaction($ins_products_cm);
// Register triggers
$ins_products_cm->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_products_cm->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_products_cm->registerTrigger("END", "Trigger_Default_Redirect", 99, "index.php?act=product&idSex={$row_detail['idSex']}");
// Add columns
$ins_products_cm->setTable("products_cm");
$ins_products_cm->addColumn("idSP", "NUMERIC_TYPE", "GET", "id");
$ins_products_cm->addColumn("noidung", "STRING_TYPE", "POST", "noidung");
$ins_products_cm->addColumn("idLogin", "NUMERIC_TYPE", "SESSION", "kt_login_user");
$ins_products_cm->addColumn("ngay_comment", "DATE_TYPE", "POST", "ngay_comment");
$ins_products_cm->addColumn("kiem_duyet", "STRING_TYPE", "POST", "kiem_duyet");
$ins_products_cm->setPrimaryKey("id_comment", "NUMERIC_TYPE");

// Make an insert transaction instance
$ins_products_cm1 = new tNG_insert($conn_conf);
$tNGs->addTransaction($ins_products_cm1);
// Register triggers
$ins_products_cm1->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert2");
$ins_products_cm1->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation1);
$ins_products_cm1->registerTrigger("END", "Trigger_Default_Redirect", 99, "index.php?act=product&idSex={detail.idSex}");
// Add columns
$ins_products_cm1->setTable("products_cm");
$ins_products_cm1->addColumn("idSP", "DATE_TYPE", "POST", "idSP");
$ins_products_cm1->addColumn("noidung", "STRING_TYPE", "POST", "noidung");
$ins_products_cm1->addColumn("idLogin", "NUMERIC_TYPE", "POST", "idLogin");
$ins_products_cm1->addColumn("ngay_comment", "DATE_TYPE", "POST", "ngay_comment");
$ins_products_cm1->addColumn("kiem_duyet", "STRING_TYPE", "POST", "kiem_duyet");
$ins_products_cm1->setPrimaryKey("id_comment", "NUMERIC_TYPE");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsproducts_cm = $tNGs->getRecordset("products_cm");
$row_rsproducts_cm = mysql_fetch_assoc($rsproducts_cm);
$totalRows_rsproducts_cm = mysql_num_rows($rsproducts_cm);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="css/index.css" rel="stylesheet" type="text/css" />
<link href="includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="includes/common/js/base.js" type="text/javascript"></script>
<script src="includes/common/js/utility.js" type="text/javascript"></script>
<script src="includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>

</head>

<body>
<p>&nbsp;</p>
<div id="detail">
<p align="left"><img src="images/<?php echo $row_detail['UrlHinh']; ?>" /></p>
<p align="center" class="style1"><?php echo $row_detail['TenSP']; ?></p>
<p align="center" class="style1"><?php echo $row_detail['MoTa']; ?> </p>
<p align="center"><span class="style1">Giá:<?php echo $row_detail['Gia']; ?> VNĐ</span></p>
<p align="center" class="style1">Ý kiến bạn đọc:</p>
<? if($totalRows_Login_cm!=0) {?>
  <table id="ykien" width="502" height="27" border="1">

<?php do { ?>
    <tr>
      <td width="492"><?php echo $row_Login_cm['noidung']; ?></td>
      </tr>
     
  <?php } while ($row_Login_cm = mysql_fetch_assoc($Login_cm)); ?>
 </table>
  <? } ?>
<form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" name="form1" id="form1">
  <table cellpadding="2" cellspacing="0" class="KT_tngtable">
    <tr>
      <td><label for="noidung"></label>
          <textarea  name="noidung" id="noidung" cols="50" rows="3"><?php echo KT_escapeAttribute($row_rsproducts_cm['noidung']); ?> </textarea>
          <input type="submit" name="KT_Insert2" id="KT_Insert2" value="Send" />
        <?php echo $tNGs->displayFieldHint("noidung");?><?php echo $tNGs->displayFieldError("products_cm", "noidung"); ?> </td>
    </tr>
  </table>
  <input type="hidden" name="idSP" id="idSP" value="<?php echo KT_formatDate($row_detail['idSP']); ?>" />
  <input type="hidden" name="idLogin" id="idLogin" value="<?php echo KT_escapeAttribute($_SESSION['kt_login_id']); ?>" />
  <input type="hidden" name="ngay_comment" id="ngay_comment" value="<?php echo date('Y,m,d'); ?>" />
  <input type="hidden" name="kiem_duyet" id="kiem_duyet" value="1" />
</form>
<p align="center" class="style1"></div>
<?php
	echo $tNGs->getErrorMsg();
?></body>
</html>
<?php
mysql_free_result($detail);

mysql_free_result($Login_cm);
?>
