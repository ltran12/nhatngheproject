-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Aug 05, 2011 at 05:09 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `quanao`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `ads`
-- 

CREATE TABLE `ads` (
  `idQC` int(4) NOT NULL auto_increment,
  `MoTa` varchar(255) NOT NULL default '',
  `Url` varchar(255) NOT NULL default '',
  `urlHinh` varchar(255) NOT NULL default '',
  `idLT` int(11) NOT NULL default '0',
  `idViTri` int(4) NOT NULL default '1',
  `SoLanClick` int(4) NOT NULL default '0',
  PRIMARY KEY  (`idQC`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

-- 
-- Dumping data for table `ads`
-- 

INSERT INTO `ads` VALUES (42, 'LOGO ADIDAS', 'http://www.adidas.com', 'adidas_1.jpg', 0, 1, 0);
INSERT INTO `ads` VALUES (43, 'LOGO BURBERRY', 'http://www.burberry.com', 'burberry_1.jpg', 0, 2, 0);
INSERT INTO `ads` VALUES (44, 'LOGO KAPPA', 'http://www.kappa.com', 'kappa_1.jpg', 0, 3, 0);
INSERT INTO `ads` VALUES (45, 'LOGO PUMA', 'http://www.puma.com', 'puma_1.jpg', 0, 4, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `loai`
-- 

CREATE TABLE `loai` (
  `idLoai` int(12) NOT NULL auto_increment,
  `TenLoai` varchar(100) NOT NULL,
  `ThuTu` int(4) NOT NULL default '1',
  `AnHien` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`idLoai`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `loai`
-- 

INSERT INTO `loai` VALUES (1, 'Áo ngắn tay', 1, 1);
INSERT INTO `loai` VALUES (2, 'Áo dài tay', 1, 1);
INSERT INTO `loai` VALUES (3, 'Quần ngắn', 1, 1);
INSERT INTO `loai` VALUES (4, 'Quần Jean', 1, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `login`
-- 

CREATE TABLE `login` (
  `idUser` int(11) NOT NULL auto_increment,
  `HoTen` varchar(100) NOT NULL default '',
  `Username` varchar(50) NOT NULL default '',
  `Password` varchar(32) NOT NULL,
  `DiaChi` varchar(255) NOT NULL,
  `Dienthoai` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL default '',
  `NgayDangKy` date NOT NULL default '0000-00-00',
  `idGroup` int(11) NOT NULL default '0',
  `NgaySinh` date default NULL,
  `GioiTinh` tinyint(1) default NULL,
  `Active` int(11) default NULL,
  `RandomKey` varchar(255) default NULL,
  `LoginNumber` int(11) default '0',
  `DisableDate` datetime NOT NULL,
  `Expiration` int(4) NOT NULL,
  PRIMARY KEY  (`idUser`),
  UNIQUE KEY `Username` (`Username`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `login`
-- 

INSERT INTO `login` VALUES (1, 'lam', 'lam', '202cb962ac59075b964b07152d234b70', 'a', '32', '34@aaa', '0000-00-00', 0, NULL, NULL, 1, NULL, 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `luottruycap`
-- 

CREATE TABLE `luottruycap` (
  `dem` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `luottruycap`
-- 

INSERT INTO `luottruycap` VALUES (0);

-- --------------------------------------------------------

-- 
-- Table structure for table `nsx`
-- 

CREATE TABLE `nsx` (
  `idNSX` int(12) NOT NULL auto_increment,
  `TenNSX` varchar(100) NOT NULL,
  `ThuTu` int(4) NOT NULL default '1',
  `AnHien` int(1) NOT NULL default '1',
  `LogoNSX` varchar(50) default NULL,
  PRIMARY KEY  (`idNSX`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `nsx`
-- 

INSERT INTO `nsx` VALUES (1, 'Holister', 1, 1, NULL);
INSERT INTO `nsx` VALUES (2, 'Abercrombie', 1, 1, NULL);
INSERT INTO `nsx` VALUES (3, 'American Eagle', 1, 1, NULL);
INSERT INTO `nsx` VALUES (4, 'Zana', 1, 1, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `products`
-- 

CREATE TABLE `products` (
  `idSP` int(12) NOT NULL auto_increment,
  `idLoai` int(12) NOT NULL default '1',
  `idNSX` int(2) NOT NULL default '4',
  `TenSP` varchar(100) NOT NULL default 'model 1',
  `MoTa` varchar(255) default NULL,
  `NgayCapNhat` date NOT NULL default '2011-07-22',
  `Gia` int(4) NOT NULL default '100000',
  `UrlHinh` varchar(255) NOT NULL default 'polo-nam-1.png',
  `baiviet` text,
  `SoLanXem` int(4) default '0',
  `SoLuongTonKho` int(4) default '0',
  `GhiChu` varchar(255) default NULL,
  `SoLanMua` int(4) default '0',
  `AnHien` int(1) default '1',
  `idSex` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`idSP`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

-- 
-- Dumping data for table `products`
-- 

INSERT INTO `products` VALUES (24, 1, 4, 'model 23', NULL, '2011-07-22', 100000, 'nam 1.jpg', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (26, 1, 4, 'model 25', NULL, '2011-07-22', 100000, 'nam 3.jpg', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (27, 0, 0, 'model 1', 'Giáº£m giÃ¡ khuyáº¿n mÃ£i cÃ²n 69,000 VNÄ', '2011-07-22', 100000, 'nam 4.jpg', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (28, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nam 5.jpg', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (29, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nam 6.jpg', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (30, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nam 7.png', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (31, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nam 8.png', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (32, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nam 9.png', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (35, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-1.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (37, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-3.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (38, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-4.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (39, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-5.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (40, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-5.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (41, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-6.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (42, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-6.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (43, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-7.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (44, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-7.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (45, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-8.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (46, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-8.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (47, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-9.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (49, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-10.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (50, 1, 4, 'model 1', NULL, '2011-07-22', 100000, 'nu-11.png', NULL, 0, 0, NULL, 0, 1, 0);
INSERT INTO `products` VALUES (51, 1, 4, 'polo-nam-1', NULL, '2011-07-22', 100000, 'polo-nam-1.png', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (53, 1, 4, 'polo-nam-3', NULL, '2011-07-22', 100000, 'polo-nam-3.png', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (54, 1, 4, 'polo-nam-4', NULL, '2011-07-22', 100000, 'polo-nam-4.png', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (55, 1, 4, 'polo-nam-5', NULL, '2011-07-22', 100000, 'polo-nam-5.jpg', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (56, 1, 4, 'polo-nam-6', NULL, '2011-07-22', 100000, 'polo-nam-6.jpg', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (57, 1, 4, 'polo-nam-8', NULL, '2011-07-22', 100000, 'polo-nam-8.jpg', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (59, 1, 4, 'polo-nam-9', NULL, '2011-07-22', 100000, 'polo-nam-9.jpg', NULL, 0, 0, NULL, 0, 1, 1);
INSERT INTO `products` VALUES (60, 1, 4, 'polo-nam-10', NULL, '2011-07-22', 100000, 'polo-nam-10.jpg', NULL, 0, 0, NULL, 0, 1, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `products_cm`
-- 

CREATE TABLE `products_cm` (
  `id_comment` int(11) NOT NULL auto_increment,
  `idLogin` int(11) NOT NULL,
  `idSP` int(11) NOT NULL,
  `noidung` text NOT NULL,
  `ngay_comment` date default NULL,
  `kiem_duyet` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id_comment`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `products_cm`
-- 

INSERT INTO `products_cm` VALUES (1, 0, 53, 'b', '0000-00-00', 0);
INSERT INTO `products_cm` VALUES (2, 0, 54, 'c', '0000-00-00', 0);
INSERT INTO `products_cm` VALUES (3, 0, 60, 'abc', '0000-00-00', 1);
INSERT INTO `products_cm` VALUES (4, 1, 57, ' Ä‘áº¹p   ', '0000-00-00', 1);
INSERT INTO `products_cm` VALUES (5, 1, 57, ' Ä‘áº¹p hÃ´ng máº¥y báº¡n?', '0000-00-00', 1);
INSERT INTO `products_cm` VALUES (6, 1, 57, ' Ä‘áº¹p nÃ¨', '0000-00-00', 1);
INSERT INTO `products_cm` VALUES (7, 1, 53, ' cÃ¡i Ã¡o nÃ y sao cÅ©ng Ä‘áº¹p ta', '0000-00-00', 1);
INSERT INTO `products_cm` VALUES (8, 1, 53, ' Ä‘áº¹p mÃ ', '0000-00-00', 1);
INSERT INTO `products_cm` VALUES (9, 1, 28, ' thix cÃ¡i Ã¡o nÃ y nÃ¨', '0000-00-00', 1);
